#!/usr/bin/python

#Import some modules
import re
import requests
import json
from datetime import datetime
import time
import argparse
import sys
import PyGnuplot as gp

##### Parse the arguments
def parseArgs():
  parser = argparse.ArgumentParser(description='Parse Ripple Ledger')
  parser.add_argument('--unique', help="Max unique sequence numbers to loop through eg: 5", type=int)
  parser.add_argument('--secs', help="Number of seconds to run for regardless of duplicate sequence numbers eg: 15", type=int)
  parser.add_argument('--url', help="Specify the url", type=str, default="https://s1.ripple.com:51234")
  parser.add_argument('--outputFile', help="Where to output the data eg: validated_ledger.dat", type=str, default="validated_ledger.dat", required=True)
  parser.add_argument('--plot', help="Plot the output to gnuplot (make sure your DISPLAY is exported", action='store_true')

  if len(sys.argv)==1:
      parser.print_help(sys.stderr)
      sys.exit(1)

  args = parser.parse_args()

  #print(args)

  return args

##### Set variables based on the command line args
# How many itterations will it run
def setVars(theArgs, startVal=1, preRipValSeqVal=0):

  start=startVal

  if theArgs.unique:
    end=theArgs.unique

  if theArgs.secs:
    end=theArgs.secs

  preRipValSeq=preRipValSeqVal

  return start, end, preRipValSeq

##### Create or clear the specified filename on the command line
def fileSetup(args):
  filename = args.outputFile
  try:
      f = open(filename, "w").close
  except IOError:
      print("Unable to create file on disk")
      sys.exit(1)

  return filename

##### Set up the url and payload
def setUrlData(args):
  # Specify the Ripple server and json data
  url = args.url 
  payload = {"jsonrpc": "2.0", "id": 2, "method": "server_info", "params": [{}]}
  headers = {'content-type': 'application/json'}

  return url, payload, headers

##### Send the payload and get a response back
def openUrl(url, payload, headers):
  try:
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    response.raise_for_status()
  except requests.exceptions.HTTPError as err:
    raise SystemExit(err)
  except requests.exceptions.Timeout:
    print("Timed out")
    sys.exit(1)
  except requests.exceptions.TooManyRedirects:
    print("Bad URL?")
    sys.exit(1)
  except requests.exceptions.RequestException as e:
    print("Exception encountered - Something horrible happened - Bailing out")
    raise SystemExit(e)
    sys.exit(1)

  return response

##### Get the data we need i.e: Sequence number and Time
def parseJsonData(response):
  json_data = json.loads(response.text)

  ripValSeq=(json_data['result']['info']['validated_ledger']['seq'])
  ripValTime=(json_data['result']['info']['time'])

  return ripValSeq, ripValTime

##### Add a timezone if there isn't one
# This is to fix an issue where sometimes the timezone is not reported  in the server_info output
def cleanseTz(ripValTime, tz="UTC"):

  tzSanitized = "no"

  if tz not in ripValTime:
    ripValTimeWithTz=ripValTime + " " +  tz
    tzSanitized = "yes"
  else:
    ripValTimeWithTz=ripValTime
    utcSanitzed = "no"

  return ripValTimeWithTz, tzSanitized, tz

##### Convert the time into something more easy to do calculations with like the epoch date
def setEpochDate(ripValTimeWithTz, tz):
  pattern = '%Y-%b-%d %H:%M:%S.%f' + " " + tz
  epoch = int(time.mktime(time.strptime(ripValTimeWithTz, pattern)))

  return epoch

##### Work out how many lines there are in the file so we can do some maths with it
def fileNumLines(filename):
  
  numLines = 0

  for line in open(filename).readlines(  ):
    numLines += 1

  return numLines

##### Get a list of all the epochs (last field) from the output file
def getEpochs(filename):

  epochList = []

  for line in open(filename).readlines(  ):
    epochList.append(line.split(" ")[-1])

  return epochList

##### Use the list of epochs to calculate the average ledger update time
def calcAverage(numLines, epochList):

  epochDiffsTotal = 0
  # Start from second item in the list to avoid false output from the first entry
  itemEpochList = 1

  if numLines < 2:
    print("Number of lines is less than 2 - Unable to calculate the Average")
  else:

    # Subtract 1 as 1) elements start from 0 in python and 2) we want to be able to add n+1
    lenEpochList = len(epochList) - 1

    while itemEpochList < lenEpochList:
      epochNumOne = int(epochList[itemEpochList].strip())
      epochNumTwo = int(epochList[itemEpochList + 1].strip())
      epochDiff = epochNumTwo - epochNumOne
      epochDiffsTotal = epochDiffsTotal + epochDiff
      itemEpochList += 1
  
  # Take 1 away from numLines as we are not including the first line in our data
  epochAverage = epochDiffsTotal / (numLines - 1)

  return epochAverage

##### Use the list of epochs to calculate the minimum ledger update time
def calcMin(numLines, epochList):

  epochMinDiff = 999999999999
  # Start from second item in the list to avoid false output from the first entry
  itemEpochList = 1

  if numLines < 2:
    print("Number of lines is less than 2 - Unable to calculate the Min")
  else:

    # Subtract 1 as 1) elements start from 0 in python and 2) we want to be able to add n+1
    lenEpochList = len(epochList) - 1

    while itemEpochList < lenEpochList:
      epochNumOne = int(epochList[itemEpochList].strip())
      epochNumTwo = int(epochList[itemEpochList + 1].strip())
      epochDiff = epochNumTwo - epochNumOne

      if epochDiff < epochMinDiff:
        epochMinDiff = epochDiff

      itemEpochList += 1

  return epochMinDiff

##### Use the list of epochs to calculate the maximum ledger update time
def calcMax(numLines, epochList):

  epochMaxDiff = 0
  # Subtract 1 as 1) elements start from 0 in python and 2) we want to be able to add n+1
  itemEpochList = 1

  if numLines < 2:
    print("Number of lines is less than 2 - Unable to calculate the Min")
  else:

    # Subtract 1 as 1) elements start from 0 in python and 2) we want to be able to add n+1
    lenEpochList = len(epochList) - 1

    while itemEpochList < lenEpochList:
      epochNumOne = int(epochList[itemEpochList].strip())
      epochNumTwo = int(epochList[itemEpochList + 1].strip())
      epochDiff = epochNumTwo - epochNumOne

      if epochDiff > epochMaxDiff:
        epochMaxDiff = epochDiff

      itemEpochList += 1

  return epochMaxDiff

##### use gnuplot to plot the plot the times of the sequence numbers
def plotIt(filename):

  gp.c('reset')
  gp.c('pause 1')
  gp.c('set term x11')
  gp.c('set xdata time')
  gp.c('set xlabel "Time"')
  gp.c('set ylabel "Sequence"')
  gp.c('set timefmt "%Y-%b-%d %H:%M:%S"')
  gp.c('set format x "%b/%d-%H:%M:%S"')
  # Have to create the full output as the plot command strips the file extension
  #plotCmd = 'plot "' + filename + '" using 1:4'
  #plotCmd = 'plot "' + filename + '" using 1:4:(sprintf("(%d)", $4)) with labels notitle)'
  plotCmd = 'plot "' + filename + '" using 1:4:4 with labels point'

  try:
    gp.c(plotCmd)
  except:
    print("Gnuplot exception - Have to set your DISPLAY variable? and got an Xserver installed?")
    sys.exit(1)


##### Print the Average/Max/Min stats
def printStats(numLines, epochsList, end):

  print "\n==================================================\n"
  averageTime = calcAverage(numLines, epochsList)
  print("Average ledger update time across {} itterations = {}".format(end,averageTime))

  minTime = calcMin(numLines, epochsList)
  print("Minimum ledger update time across {} itterations = {}".format(end,minTime))

  maxTime = calcMax(numLines, epochsList)
  print("Maximum ledger update time across {} itterations = {}".format(end,maxTime))
  print "\n==================================================\n"

##### Write the data to a file
def writeFile(filename, ripValTimeWithTz, ripValSeq, epoch):
  f = open(filename, "a")
  f.write(str(ripValTimeWithTz) + " " + str(ripValSeq) + " " + str(epoch) + "\n")
  f.close()


########## MAIN FUNCTION ############
def main():

  print "\n==================================================\n"

# Get the arguments
  cl_args = parseArgs()
# Set a baseline number for start/end itterations/seconds and sequence number
  start, end, preRipValSeq = setVars(cl_args)
# Get the filename
  filename = fileSetup(cl_args)
# Get the URL and payload information
  url, payload, headers = setUrlData(cl_args)

# Two options
#  - Run through "unique" sequence number
#  - Or run through a number of seconds not worrying about the sequence numbers
# Loop through from start until you hit the end seconds/sequence itterations, Each time:
  while start < (end + 1):
#  - Refresh the URL data
    response = openUrl(url, payload, headers)
#  - Get a new sequence and time
    ripValSeq, ripValTime = parseJsonData(response)
#  - Make sure the timezone is in the output
    ripValTimeWithTz, tzSanitized, tz = cleanseTz(ripValTime, "UTC")
#  - Generate an epoch time based on the time for use later in calulating average/min/max 
    epoch = setEpochDate(ripValTimeWithTz, tz)

#  - If "unique" sequence numbers:
    if cl_args.unique:
#    - Wait for the sequence number to change (pass and loop round again if the sequence number is the same)
      if preRipValSeq == ripValSeq:
        pass
      else:
#    - Then print the new sequence number with the time
        preRipValSeq=ripValSeq
#    - Wite the timestamp, sequence number and epoch time to the data file
        print("Run {0} sequence number {1} - Writing to {2} - TZ Sanitized: {3}".format(start,preRipValSeq,filename,tzSanitized))
#    - Wite the timestamp, sequence number and epoch time to the data file
        writeFile(filename, ripValTimeWithTz, ripValSeq, epoch)
        #f = open(filename, "a")
        #f.write(str(ripValTimeWithTz) + " " + str(ripValSeq) + " " + str(epoch) + "\n")
        #f.close()
        start += 1

#    - (re)plot the graph each time the file is written to
        if cl_args.plot:
          plotIt(filename)

#  - If number of "secs":
    if cl_args.secs:
#    - Output timestamp, sequence number and epoch time to the data file regardless of change
      print("Run {0} sequence number {1} - Writing to {2} - TZ Sanitized: {3}".format(start,ripValSeq,filename,tzSanitized))
#    - Wite the timestamp, sequence number and epoch time to the data file
      writeFile(filename, ripValTimeWithTz, ripValSeq, epoch)
      #f = open(filename, "a")
      #f.write(str(ripValTimeWithTz) + " " + str(ripValSeq) + " " + str(epoch) + "\n")
      #f.close()
      time.sleep(1)
      start += 1

      if cl_args.plot:
        plotIt(filename)

#  - Finally work out the number of lines and get a list of epoch times
  numLines = fileNumLines(filename)
  epochsList = getEpochs(filename)
#  - Then print the average/min/max difference in values
  printStats(numLines, epochsList, end)


if __name__ == "__main__":
  main()


